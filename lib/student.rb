require 'byebug'
class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_accessor :first_name, :last_name, :courses


  def name
    @first_name + " " + @last_name
  end

  def courses
    @courses
  end

  def enroll(course)
    has_conflict?(course)
    if !course.students.include?(self)
      course.students << self
      @courses << course
    end
  end

  def has_conflict?(new_course)
    @courses.each do |c|
      if c.conflicts_with?(new_course)
        raise
      end
    end
  end


  def course_load
    hash = Hash.new(0)
    @courses.each do |cc|
      hash[cc.department] += cc.credits
    end
    hash
  end

end
