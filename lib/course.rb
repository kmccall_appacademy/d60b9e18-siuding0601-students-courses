class Course

  def initialize(name, department, credits, days, time_block)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

  attr_accessor :name, :department, :credits, :students, :days, :time_block

  def students
    @students
  end


  def add_student(student)
    @students << student
    student.enroll(self)
  end

  def conflicts_with?(second_course)
    if self.time_block != second_course.time_block
      return false
    elsif self.days & second_course.days == []
      return false
    else
      return true
    end
  end



end
